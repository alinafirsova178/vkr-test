const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;
const path = require('path');
const Benchmark = require('benchmark');
const suite = new Benchmark.Suite;

const typedArrayTesting = async () => {
	let answer;
	await suite.add('array', () => {
		const arr = [];
		for (let i = 0x10000 - 1; i >= 0; --i) {
			arr[Math.random() * 0x10000 | 0] = (Math.random() * 0x100000000) | 0;
		}
	})

		.add('typed-array', () => {
			const Uint32 = new Uint32Array(0x10000);
			for (let i = 0x10000 - 1; i >= 0; --i) {
				Uint32[Math.random() * 0x10000 | 0] = (Math.random() * 0x100000000) | 0;
			}
		})
		.on('complete', function () {

			answer =  `Fastest is ${this.filter('fastest').map('name')}`;
		}).run();
		return answer;
};

app.listen(PORT);
app.use(express.static('files'));
app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Methods", "GET, PUT, PATCH, POST, DELETE");
	res.header("Access-Control-Allow-Headers", "Content-Type");
	res.header("X-Author", "itmo287671");
	next();
});
app.get('/', (req, res) => res.sendFile(path.join(__dirname, 'index.html')));

app.get('/typed-arrays', async (req, res) => {
	res.send(await typedArrayTesting());
})

module.exports.typedArrayTesting = typedArrayTesting;