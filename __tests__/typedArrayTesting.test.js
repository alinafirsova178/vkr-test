/**
 * @jest-environment node
 */

const { typedArrayTesting} = require("../app.js");

test("should return 'Fastest is typed-array'", async () => {
  const data = await typedArrayTesting();
  expect(data).toBe('Fastest is typed-array');
});